import { Component, OnInit } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { TranslateService } from '@ngx-translate/core';
import { AssetsService } from 'src/app/Services/assets.service';
import { AuthserviceService } from 'src/app/Services/authservice.service';
import { Asset } from 'src/app/shared/asset';
import { Assetprice } from 'src/app/shared/assetprice';
import { Currency } from 'src/app/shared/currency';
import { lang } from '../header/header.component';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
  styleUrls: ['./agregar.component.sass']
})
export class AgregarComponent implements OnInit {
  lang=lang;
  supportedLanguages = ['en', 'es'];
  helper = new JwtHelperService();
  decodedToken:any;
  assets: Asset[] | undefined;
  assetsprice : Assetprice | undefined;
  model = {currency:''};
  currencyadded = {name:"",symbol:"",price:0, userId:1}
  loged!: boolean;
  constructor(private translateService: TranslateService,
    private assetsService: AssetsService,
    private authservice: AuthserviceService) {
    this.translateService.addLangs(this.supportedLanguages);
    this.translateService.setDefaultLang('es');
    this.translateService.use(this.lang);
   }

  ngOnInit(){
    if (localStorage.getItem("ACCESS_TOKEN")){
      this.loged = true
    }else{
      this.loged = false
    }
    this.assetsService.getMonedas()
    .subscribe(data =>{
      this.assets = data.content;
      console.log(this.assets)
    })
    this.assetsService.getMonedasPrice()
    .subscribe(data =>{
      this.assetsprice = data.content;
      console.log(this.assetsprice)
    })
  }

  onSubmit() {
    console.log(this.model.currency)
    this.currencyadded = this.assetsService.fill(this.model.currency,this.assets,this.assetsprice);
    this.decodedToken = this.helper.decodeToken(this.authservice.token);
    console.log(this.decodedToken.usuarioId)
    this.currencyadded.userId=this.decodedToken.usuarioId//this.authservice.decodeToken();
    this.assetsService.addCurrency(this.currencyadded).subscribe(data =>{
      this.currencyadded =data;
      console.log(this.currencyadded);
    })
  }

}
