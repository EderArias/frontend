import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AssetsService } from 'src/app/Services/assets.service';
import { Asset } from 'src/app/shared/asset';
import { Assetprice } from 'src/app/shared/assetprice';

import { Conversion, Listamonedas } from '../../shared/conversion';
import { lang } from '../header/header.component';
@Component({
  selector: 'app-cambio',
  templateUrl: './cambio.component.html',
  styleUrls: ['./cambio.component.sass']
})
export class CambioComponent implements OnInit {
  form!: FormGroup; 
  conversion= {entrada:0,valconvertir:"",valconvertido:"",resultado:0}
  listamoneda = Listamonedas;
  lang=lang;
  supportedLanguages = ['en', 'es'];
  assets: Asset[] | undefined;
  currencyFromValue!:number;
  currencyToValue!:number;
  currency = {name:"",symbol:"",price:0};
  result = 0;
  assetsprice : Assetprice | undefined;

  constructor(private fb: FormBuilder,
    private formBuilder: FormBuilder,
    private translateService: TranslateService,
    private assetsService: AssetsService,
    private activeRoute: ActivatedRoute) {
    

    
    this.translateService.addLangs(this.supportedLanguages);
    this.translateService.setDefaultLang('es');
    this.translateService.use(this.lang);
  }



  

  ngOnInit(): void {
    this.assetsService.getMonedas()
    .subscribe(data =>{
      this.assets = data.content;
      console.log(this.assets)
    })
    this.assetsService.getMonedasPrice()
    .subscribe(data =>{
      this.assetsprice = data.content;
      console.log(this.assetsprice)
    })
  }

  compare(arg1:any,arg2:any){
    let ret =0
    const as = arg2.length
    for(let i = 0; i<as;i++){
      if (arg1==arg2[i].assetId){
        ret=arg2[i].price
      }
    }
    return ret
  }

  onSubmit() {
    this.currencyFromValue=this.compare(this.conversion.valconvertir,this.assetsprice);

    console.log(this.currencyFromValue);
    this.currencyToValue=this.compare(this.conversion.valconvertido,this.assetsprice);
    console.log(this.currencyToValue);
    this.conversion.resultado=(this.currencyFromValue/this.currencyToValue)*this.conversion.entrada;
    //this.currencyFromValue = this.conversionForm.value;
    //console.log(this.conversion);
    //this.conversionForm.reset();
  }

  exchangeValues(): void{
    const aux = this.currencyFromValue;
    this.currencyFromValue=this.currencyToValue;
    this.currencyToValue=aux;
    this.onSubmit();
  }
}
