import { Router } from '@angular/router';
import { AuthserviceService } from './../../Services/authservice.service';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { LoginComponent } from '../login/login.component';
//import {logd } from '../login/login.component';
var lan="";
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit {

  supportedLanguages = ['es','en' ];
  //loged!:boolean
  
  
  loged!:boolean;
  
  constructor(public dialog: MatDialog,
    private translateService: TranslateService,
    private authservice:AuthserviceService,
    private router:Router) { 
      this.translateService.addLangs(this.supportedLanguages);
    this.translateService.setDefaultLang('es');
  }

  SelectLang(lang: string){
    this.translateService.use(lang);
    lan=lang;
  }

  Logout(){
    this.authservice.logout();
    this.router.navigateByUrl('/registrarse')
  }

  
  ngOnInit()  {
    if (localStorage.getItem("ACCESS_TOKEN")){
      this.loged = true
    }else{
      this.loged = false
    }
    
  }

  openLoginForm() {
    this.dialog.open(LoginComponent, {width: '400px', height: '450px'});
  }

}

export const lang=lan;