import { AuthserviceService } from './../../Services/authservice.service';
import { AssetsService } from './../../Services/assets.service';
import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Currencyl } from 'src/app/shared/currencyl';
import { lang } from '../header/header.component';
import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.sass']
})
export class ListaComponent implements OnInit {
  lang=lang;
  supportedLanguages = ['en', 'es'];
  helper = new JwtHelperService();
  decodedToken:any;
  displayedColumns: string[] = ['Name', 'Price', 'Crypto', 'Convert'];
  dataSource : Currencyl[] = [];
  constructor(private translateService: TranslateService,
    private assetservice:AssetsService,
    private authservice: AuthserviceService) {
    this.translateService.addLangs(this.supportedLanguages);
    this.translateService.setDefaultLang('es');
    this.translateService.use(this.lang);
   }
   

  ngOnInit(): void {
    if (localStorage.getItem("ACCESS_TOKEN")){
      this.decodedToken = this.helper.decodeToken(this.authservice.token);
      this.assetservice.getCurrencies().subscribe(currencies =>{
        this.dataSource = currencies
      })
    }
  }

}
