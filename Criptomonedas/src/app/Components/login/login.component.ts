import { Router, Routes } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { AuthserviceService } from './../../Services/authservice.service';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { TranslateService } from '@ngx-translate/core';
import { lang } from '../header/header.component';
var loged = false;
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

  user = {username: '', password: ''};
  lang=lang;
  token="";
  supportedLanguages = ['en', 'es'];
  constructor(public dialogRef: MatDialogRef<LoginComponent>,
              private authService:AuthserviceService,
              private translateService: TranslateService, private router: Router) {
                this.translateService.addLangs(this.supportedLanguages);
    this.translateService.setDefaultLang('es');
    this.translateService.use(this.lang);
               }

  ngOnInit() {
  }
  

  onSubmit() {
    //this.authService.login(this.user).subscribe(data =>{
    //  this.token =data.succes;
    //  console.log(this.token);})
    //  this.logedIn(this.token);
    this.authService.login(this.user).subscribe(res =>{
      if (res.succes){
        console.log(this.user)
      console.log(this.authService.token)
      this.router.navigateByUrl('/home/lista')
      }
      });
    console.log('User: ', this.user);
    this.dialogRef.close();
  }

  //logedIn(token:any){
    //if (token){
      //loged=true;
      //window.sessionStorage.setItem(token,token)
    //}
  //}

}

