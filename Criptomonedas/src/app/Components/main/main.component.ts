import { Component, OnInit,ViewEncapsulation } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { lang } from '../header/header.component';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.sass'],
  encapsulation: ViewEncapsulation.None
})
export class MainComponent implements OnInit {
  links = [{path: 'cambio', label: 'REALIZAR CAMBIO'}, {path: 'lista', label: 'LISTA DE MONEDAS'}];
  activeLink = this.links[0];
  lang=lang;
  supportedLanguages = ['en', 'es'];
  loged!: boolean;
  constructor(private translateService: TranslateService) {
    this.translateService.addLangs(this.supportedLanguages);
    this.translateService.setDefaultLang('es');
    this.translateService.use(this.lang);
   }
  
  ngOnInit(){
    if (localStorage.getItem("ACCESS_TOKEN")){
      this.loged = true
    }else{
      this.loged = false
    }
  }

}
