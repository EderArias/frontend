import { AssetsService } from './../../Services/assets.service';
import { UsersService } from './../../Services/users.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

import { Registro } from '../../shared/registro';
import { lang } from '../header/header.component';
import { Asset } from 'src/app/shared/asset';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.sass']
})
export class RegistroComponent implements OnInit {
  hide=true;
  registroForm!: FormGroup;
  registro!: Registro;
  lang=lang;
  supportedLanguages = ['en', 'es'];
  assets: Asset[] | undefined;
  validPattern = "^[a-zA-Z0-9]$";
  
  formErrors:any = {
    'name': '' ,
    'lastname': '',
    'username': '',
    'password': '',
    'favcoin': ''
  };

  validationMessages:any = {
    'name': {
      'required':      'Name is required.'
      
    },
    'lastname': {
      'required':      'Last Name is required.'
     
    },
    'username': {
      'required':      'Username is required.',
      
    },
    'password': {
      'required':      'Password is required.',
      'pattern':       'Password must be alphanumeric',
      'minlength':     'Password must be at least 8 characters long.'
    },
    'favcoin': {
      'required':      'Favorite Currency is required.'
      
    },
  };
  constructor(private fb: FormBuilder,
    private translateService: TranslateService,
    private usersService: UsersService,
    private assetsService: AssetsService) { 
    this.createForm();
    this.translateService.addLangs(this.supportedLanguages);
    this.translateService.setDefaultLang('es');
    this.translateService.use(this.lang);
  }

  ngOnInit(): void {
    this.assetsService.getMonedas()
    .subscribe(data =>{
      this.assets = data.content;
      console.log(this.assets)
    })
  }
  
  createForm() {
    this.registroForm = this.fb.group({
      name: ['', [Validators.required] ],
      lastname: ['', [Validators.required] ],
      username: ['', [Validators.required] ],
      password: ['', [Validators.required, Validators.minLength(8), Validators.pattern] ],
      favcoin: ['', [Validators.required] ]
    });

    this.registroForm.valueChanges
      .subscribe(data => this.onValueChanged(data));

    this.onValueChanged();
  }

  onValueChanged(data?: any) {
    
    if (!this.registroForm) { return; }
    const form = this.registroForm;
    for (const field in this.formErrors) {
      if (this.formErrors.hasOwnProperty(field)) {
        // clear previous error message (if any)
        this.formErrors[field] = '';
        const control = form.get(field);
        if (control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          for (const key in control.errors) {
            if (control.errors.hasOwnProperty(key)) {
              this.formErrors[field] += messages[key] + ' ';
            }
          }
        }
      }
    }
  }

  onSubmit() {
    this.registro = this.registroForm.value;
    this.usersService.addUser(this.registro).subscribe(data =>{
    this.registro =data;
    console.log(this.registro);
    });
    this.registroForm.reset();
  }

}
