import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopMonedasComponent } from './top-monedas.component';

describe('TopMonedasComponent', () => {
  let component: TopMonedasComponent;
  let fixture: ComponentFixture<TopMonedasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TopMonedasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TopMonedasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
