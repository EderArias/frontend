import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AssetsService } from 'src/app/Services/assets.service';
import { AuthserviceService } from 'src/app/Services/authservice.service';
import { Currencyl } from 'src/app/shared/currencyl';
import { lang } from '../header/header.component';

@Component({
  selector: 'app-top-monedas',
  templateUrl: './top-monedas.component.html',
  styleUrls: ['./top-monedas.component.sass']
})
export class TopMonedasComponent implements OnInit {
  lang=lang;
  supportedLanguages = ['en', 'es'];
  loged!: boolean;
  dataSource : Currencyl[] = [];
  displayedColumns: string[] = ['Name', 'Price', 'Crypto'];
  constructor(private translateService: TranslateService,
    private assetsService: AssetsService,
    private authservice: AuthserviceService) {
      this.translateService.addLangs(this.supportedLanguages);
      this.translateService.setDefaultLang('es');
      this.translateService.use(this.lang); }

  ngOnInit() {
    if (localStorage.getItem("ACCESS_TOKEN")){
      this.loged = true
      this.assetsService.getTopCurrencies().subscribe(currencies =>{
        this.dataSource = currencies
      })
    }else{
      this.loged = false
    }

  }

}
