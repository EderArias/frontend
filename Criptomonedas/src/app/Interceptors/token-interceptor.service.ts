import { Injectable, Injector } from '@angular/core';
import { AuthserviceService } from '../Services/authservice.service';
import { HttpInterceptor } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor{

  constructor(private injector:Injector) { }

  intercept(req: { clone: (arg0: { setHeaders: { token: string; }; }) => any; },next: { handle: (arg0: any) => any; }){
    let authService = this.injector.get(AuthserviceService)
    let tokenizedReq = req.clone({
      
      setHeaders:{
        token: `${authService.getToken()}` 
      }
    })
    return next.handle(tokenizedReq)
  }
}