import { AuthserviceService } from './authservice.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { baseUrl } from 'src/environments/environment';
import { Currency } from '../shared/currency';
import { JwtHelperService } from '@auth0/angular-jwt';
@Injectable({
  providedIn: 'root'
})
export class AssetsService {
  currency = {name:"",symbol:"",price:0, userId:0};
  helper = new JwtHelperService();
  decodedToken:any;
  constructor(private http: HttpClient,
    private authservice:AuthserviceService) { }
  getMonedas():Observable<any> {
    
    return this.http.get<any>(`${baseUrl}assets/getassets`);
  }

  getMonedasPrice():Observable<any> {
    
    return this.http.get<any>(`${baseUrl}assets/getassetsprice`);
  }

  getCurrencies():Observable<any> {
    this.decodedToken = this.helper.decodeToken(this.authservice.token);
    return this.http.get<any>(`${baseUrl}monedas/monedasuser/${this.decodedToken.usuarioId}`);
  }

  getTopCurrencies():Observable<any> {
    this.decodedToken = this.helper.decodeToken(this.authservice.token);
    return this.http.get<any>(`${baseUrl}monedas/topmonedas/${this.decodedToken.usuarioId}`);
  }

  addCurrency(currency :Currency):Observable<any>{
    let iJson = JSON.stringify(currency)
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    return this.http.post<any>(`${baseUrl}monedas/setmoneda`,currency,httpOptions);
  }

  fill(currenci:any,ar1:any,ar2:any){
    const as = ar1.length;
    
    const as2 = ar2.length;
    console.log(as,as2)
    //var na = [];
    for (let i = 0; i < as; i++) {
    //na.push(ar1.content[i].id);
    if(currenci==ar1[i].id) {
      this.currency.name=ar1[i].name;
      this.currency.symbol=ar1[i].symbol;
    }
    }
    for (let i = 0; i < as2; i++) {
    if (currenci==ar2[i].assetId){
      this.currency.price=ar2[i].price
    }
    }
    return this.currency
  }
}
