import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { baseUrl } from 'src/environments/environment';
import { tap } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthserviceService {

  constructor(private http:HttpClient) { }
  loged=false;
  token!: string;
  helper = new JwtHelperService();

  login(user: any):Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    return this.http.post<any>(`${baseUrl}users/login`, user, httpOptions).pipe(tap(
      (res:any)=> {
        if(res.succes){
          this.saveToken(res.succes);
          this.loged=true;
          this.token=res.succes;
          console.log(this.loged)
          console.log(this.token)
        }else{
          this.token=''
        }
        

      })
    );
  }

  logout(){
    this.token = '';
    localStorage.removeItem("ACCESS_TOKEN");
    this.loged=false;
  }

  loginstatus(){
    console.log(this.loged)
    return this.loged;

  }

  private saveToken(token:string){
    localStorage.setItem("ACCESS_TOKEN",token)
    this.token=token;
  }

  getToken(){
    return localStorage.getItem("ACCESS_TOKEN");
  }

  decodeToken(){
    const decodedtoken = this.helper.decodeToken(this.token);
    console.log(decodedtoken)
    return decodedtoken;
  }
}
