import { Routes } from '@angular/router';

import { MainComponent } from '../Components/main/main.component';
import { RegistroComponent } from '../Components/registro/registro.component';
import { AgregarComponent } from '../Components/agregar/agregar.component';
import { CambioComponent } from '../Components/cambio/cambio.component';
import { ListaComponent } from '../Components/lista/lista.component';
import { TopMonedasComponent } from '../Components/top-monedas/top-monedas.component';



export const routes: Routes = [
  { path: 'home',  component: MainComponent,
  children: [
    { path: '', redirectTo: 'cambio', pathMatch: 'full' },
    { path: 'cambio',     component: CambioComponent },
    { path: 'lista',     component: ListaComponent },
  ]}, 
  { path: 'registrarse',     component: RegistroComponent },
  { path: 'agregar',     component: AgregarComponent },
  { path: 'topmonedas',     component: TopMonedasComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full' }
];