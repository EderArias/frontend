export class Asset {
    id!: string;
    name: string | undefined;
    symbol: string | undefined;
    status: string | undefined;
    type: string| undefined;
    url: string | undefined;
}