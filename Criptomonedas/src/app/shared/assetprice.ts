export class Assetprice {
    id!: string;
    assetId: string | undefined;
    timestamp: string | undefined;
    marketCapRank: number | undefined;
    volumeRank: number | undefined;
    price: number | undefined;
    volume: number | undefined;
    totalSupply: number | undefined;
    freeFloatSupply: number | undefined;
    marketCap: number | undefined;
    totalMarketCap: number | undefined;
}