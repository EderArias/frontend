export class Currency {
    name: string | undefined;
    symbol: string | undefined;
    price: number | undefined;
    userId: number | undefined;
}