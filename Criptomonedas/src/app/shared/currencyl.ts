export class Currencyl {
    id: number | undefined;
    name: string | undefined;
    symbol: string | undefined;
    price: number | undefined;
    userId: number | undefined;
}